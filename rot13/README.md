# ROT13

## Version

Rust or Python3

## Usage

```shell
$ ./rot13.py [rotate number]
$ python3 rot13.py [rotate number]
# or
$ cargo build --release
$ ./target/release/rot13 [rotate number]
```
