use std::{env, io, process, str};

fn exit_err(e: io::Error) -> ! {
    eprintln!("error: while reading line from stdin: {}", e);
    process::exit(1)
}

fn get_line() -> String {
    let mut line = String::new();
    match io::stdin().read_line(&mut line) {
        Ok(0) => process::exit(0),
        Err(e) => exit_err(e),
        _ => (),
    }

    line
}

fn rot13(n: i64) {
    loop {
        let line = get_line();
        let result = line
            .as_bytes()
            .iter()
            .map(|c| match c {
                b'A'..=b'Z' => ((c - b'A') as i64 + n).rem_euclid(26) as u8 + b'A',
                b'a'..=b'z' => ((c - b'a') as i64 + n).rem_euclid(26) as u8 + b'a',
                c => *c,
            })
            .collect::<Vec<_>>();

        print!("{}", str::from_utf8(&result).unwrap());
    }
}

fn main() {
    let mut args = env::args().into_iter();
    let name = args.next().unwrap();
    let n = args
        .next()
        .unwrap_or(String::from("13"))
        .parse::<i32>()
        .unwrap_or_else(|_| {
            eprintln!("Usage: {} [rotate number(signed int 32bit)]", name);
            process::exit(0)
        });

    rot13(n as i64);
}
