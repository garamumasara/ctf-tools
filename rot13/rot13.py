#!/usr/bin/env python3
import sys
from typing import NoReturn

def exit_err() -> NoReturn:
    print(f"error: while reading line from stdin: {sys.exc_info()[0]}", file=sys.stderr)
    exit(1)

def get_line() -> str:
    try:
        line = input().strip()
    except (EOFError, KeyboardInterrupt):
        exit(0)
    except:
        exit_err()

    return line

def rot13(n: int) -> None:
    while True:
        line = get_line()
        result = ""
        for c in line:
            if 'A' <= c <= 'Z':
                result += chr((ord(c) - ord('A') + n) % 26 + ord('A'))
            elif 'a' <= c <= 'z':
                result += chr((ord(c) - ord('a') + n) % 26 + ord('a'))
            else:
                result += c

        print(result)

def main() -> None:
    try:
        n = int(sys.argv[1])
    except IndexError:
        n = 13
    except:
        print(f"Usage: {sys.argv[0]} [rotate number]", file=sys.stderr)
        exit(0)

    rot13(n)

if __name__ == "__main__":
    main()
