#include <iostream>
#include <string>
#include <algorithm>
using namespace std::literals;

void exit_err() {
    std::cerr << "error: while reading line from stdin: "s << std::endl;
    exit(1);
}

std::string get_line() {
    std::string line;
    std::getline(std::cin, line);
    if (std::cin.eof()) {
        exit(0);
    } else if (std::cin.fail() || std::cin.bad()) {
        exit_err();
    }

    return line;
}

void rot13(int64_t n) {
    while (true) {
        std::string line = get_line();
        std::string result;
        for (auto &&c : line) {
            if (c >= 'A' && c <= 'Z') {
                result += ((c - 'A' + n) % 26 + 26) % 26 + 'A';
            } else if (c >= 'a' && c <= 'z') {
                result += ((c - 'a' + n) % 26 + 26) % 26 +'a';
            } else {
                result += c;
            }
        }

        std::cout << result << std::endl;
    }
}

int main(int argc, char* argv[]) {
    int n = 13;
    if (argc > 1) {
        try {
            n = std::stoi(argv[1]);
        } catch (...) {
            std::cerr << "Usage: "s << argv[0] << " [rotate number(signed int 32bit)]"s << std::endl;
            exit(0);
        }
    }

    rot13(int64_t(n));
}

